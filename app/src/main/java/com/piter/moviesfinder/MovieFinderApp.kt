package com.piter.moviesfinder

import android.app.Application
import com.piter.moviesfinder.log.DebugTree
import timber.log.Timber

class MovieFinderApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(DebugTree())
    }
}