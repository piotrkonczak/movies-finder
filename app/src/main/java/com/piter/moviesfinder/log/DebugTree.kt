package com.piter.moviesfinder.log

import timber.log.Timber

class DebugTree : Timber.DebugTree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        val tagWithPrefix = "MOVIES_$tag"
        super.log(priority, tagWithPrefix, message, t)
    }
}