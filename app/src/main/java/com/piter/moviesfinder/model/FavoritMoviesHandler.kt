package com.piter.moviesfinder.model

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class FavoriteMoviesHandler private constructor(context: Context) {

    private val PREF_NAME = "FAV_MOVIES"
    private val PREF_KEY = "IDS"
    private val sharedPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    private val favMoviesIds = loadFavMoviesIds() ?: ArrayList()

    companion object {

        @Volatile
        private var instance: FavoriteMoviesHandler? = null

        fun getInstance(context: Context) =
            instance ?: synchronized(this) {
                instance ?: FavoriteMoviesHandler(context).also { instance = it }
            }
    }

    fun updateList(id: Int) {
        if (favMoviesIds.contains(id)) {
            favMoviesIds.remove(Integer.valueOf(id))
        } else {
            favMoviesIds.add(id)
        }
        storeFavMoviesIds()
    }

    fun isFavoriteMovie(id: Int): Boolean {
        return favMoviesIds.contains(id)
    }

    private fun loadFavMoviesIds(): ArrayList<Int>? {
        val gson = Gson()
        val json = sharedPref.getString(PREF_KEY, null)
        val type: Type = object : TypeToken<ArrayList<Int>>() {}.type

        return if (json != null)
            gson.fromJson(json, type)
        else
            null
    }

    private fun storeFavMoviesIds() {
        val editor: Editor = sharedPref.edit()
        val gson = Gson()
        val json = gson.toJson(favMoviesIds)
        editor.putString(PREF_KEY, json)
        editor.apply()
    }
}