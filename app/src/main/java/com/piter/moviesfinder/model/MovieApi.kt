package com.piter.moviesfinder.model

import com.piter.moviesfinder.data.MovieResult
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieApi {

    @GET("movie/now_playing")
    fun getMovies(): Single<MovieResult>

    @GET("search/movie")
    fun searchMovie(@Query("query") query: String): Single<MovieResult>
}