package com.piter.moviesfinder.model

import com.piter.moviesfinder.data.MovieResult
import io.reactivex.Single
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class MovieApiService {

    private val BASE_URL = "https://api.themoviedb.org/3/"
    private val API_KEY = "5a3d8a3b4b4a673a14c592d08655719d"

    private val api: MovieApi

    init {
        val requestInterceptor = Interceptor { chain ->
            val url = chain.request()
                .url()
                .newBuilder()
                .addQueryParameter("api_key", API_KEY)
                .build()

            val request = chain.request()
                .newBuilder()
                .url(url)
                .build()

            return@Interceptor chain.proceed(request)
        }

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(requestInterceptor)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()

        api = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(MovieApi::class.java)
    }


    fun getMovies(): Single<MovieResult> {
        return api.getMovies()
    }

    fun searchMovie(title: String): Single<MovieResult> {
        return api.searchMovie(title)
    }


}