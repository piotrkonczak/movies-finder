package com.piter.moviesfinder.view

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Filter

class AutoCompleteMovieAdapter(context: Context, private var list: List<String>): ArrayAdapter<String>(
    context,
    android.R.layout.simple_list_item_1
) {
    override fun getFilter(): Filter {
        return filter
    }

    override fun getCount() = list.size

    override fun getItem(position: Int): String? {
        return list[position]
    }

    private val filter: Filter =
        object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val res = FilterResults()
                res.values = list
                res.count = list.size
                return res
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                clear()
                results?.let {
                    list = it.values as List<String>
                }
                notifyDataSetChanged()
            }

            override fun convertResultToString(resultValue: Any?): CharSequence {
                return resultValue.toString()
            }
        }



}