package com.piter.moviesfinder.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.piter.moviesfinder.R
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}