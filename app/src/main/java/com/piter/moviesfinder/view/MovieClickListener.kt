package com.piter.moviesfinder.view

import android.view.View

interface MovieClickListener {
    fun onClick(v: View)
    fun onFavBtClick(v: View)
}