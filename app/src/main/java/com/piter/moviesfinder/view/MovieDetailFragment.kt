package com.piter.moviesfinder.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.piter.moviesfinder.R
import com.piter.moviesfinder.databinding.FragmentMovieDetailBinding
import com.piter.moviesfinder.model.FavoriteMoviesHandler

class MovieDetailFragment : Fragment() {

    val args: MovieDetailFragmentArgs by navArgs()
    private lateinit var binding: FragmentMovieDetailBinding
    private lateinit var favoriteMoviesHandler: FavoriteMoviesHandler

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMovieDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        favoriteMoviesHandler = FavoriteMoviesHandler.getInstance(requireContext())

        binding.movie = args.movie
        binding.titleText.text = args.movie.title
        binding.okBtn.setOnClickListener {
            with(view?.findNavController()!!) {
                this.navigate(R.id.action_movieDetailFragment_to_moviesFragment)
            }
        }

        setFavButtonImage()

        binding.favBtn.setOnClickListener {
            favoriteMoviesHandler.updateList(args.movie.id)
            setFavButtonImage()
        }
    }

    private fun setFavButtonImage() {
        if (favoriteMoviesHandler.isFavoriteMovie(args.movie.id)) {
            binding.favBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_baseline_favorite_24))
        } else {
            binding.favBtn.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_baseline_favorite_border_24))
        }

    }
}