package com.piter.moviesfinder.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.piter.moviesfinder.R
import com.piter.moviesfinder.data.Result
import com.piter.moviesfinder.databinding.ItemMovieBinding
import com.piter.moviesfinder.model.FavoriteMoviesHandler
import timber.log.Timber


class MovieListAdapter(
    private val movieList: ArrayList<Result>,
    private val context: Context
) : RecyclerView.Adapter<MovieListAdapter.MovieViewHolder>(), MovieClickListener {

    private val favoriteMoviesHandler = FavoriteMoviesHandler.getInstance(context)

    class MovieViewHolder(var view: ItemMovieBinding) : RecyclerView.ViewHolder(view.root)

    fun updateNewsList(newNewsList: List<Result>) {
        movieList.clear()
        movieList.addAll(newNewsList)
        notifyDataSetChanged()
    }

    override fun getItemCount() = movieList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view =
            DataBindingUtil.inflate<ItemMovieBinding>(inflater, R.layout.item_movie, parent, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.view.movie = movieList[position]
        holder.view.listener = this

        if (favoriteMoviesHandler.isFavoriteMovie(movieList[position].id)) {
            holder.view.favBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite_24))
        } else {
            holder.view.favBtn.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_baseline_favorite_border_24))
        }
    }

    override fun onClick(v: View) {
        for (movie in movieList) {
            if (movie.id == v.tag.toString().toInt()) {
                val action = MoviesFragmentDirections.actionMoviesFragmentToMovieDetailFragment(
                    movie
                )
                Navigation.findNavController(v).navigate(action)
            }
        }
    }

    override fun onFavBtClick(v: View) {
        for (movie in movieList) {
            if (movie.id == v.tag.toString().toInt()) {
                favoriteMoviesHandler.updateList(movie.id)
                notifyDataSetChanged()
                break
            }
        }
    }
}