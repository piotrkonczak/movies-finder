package com.piter.moviesfinder.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.piter.moviesfinder.databinding.MoviesFragmentBinding
import com.piter.moviesfinder.viewmodel.MoviesViewModel
import timber.log.Timber


class MoviesFragment : Fragment() {

    private lateinit var moviesAdapter: MovieListAdapter
    private lateinit var viewModel: MoviesViewModel
    private lateinit var binding:  MoviesFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = MoviesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MoviesViewModel::class.java)
        moviesAdapter = MovieListAdapter(arrayListOf(), requireContext())

        binding.autoComplete.addTextChangedListener(
            object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

                override fun afterTextChanged(s: Editable?) {
                    if (s.toString().length > 2) {
                        viewModel.searchMovie(s.toString())
                    }
                }
            }
        )

        binding.moviesList.apply {
            layoutManager = GridLayoutManager(context, 1)
            adapter = moviesAdapter
        }

        binding.getDataBt.setOnClickListener {
            viewModel.getMovies()
        }

        viewModel.getMovieList().observe(viewLifecycleOwner, { list ->
            binding.loadProgress.visibility = View.GONE
            binding.loadErrorText.visibility = View.GONE
            binding.moviesList.visibility = View.VISIBLE
            moviesAdapter.updateNewsList(list)
        })

        viewModel.getSearchList().observe(viewLifecycleOwner, { list ->
            val titleList = ArrayList<String>()
            for (movie in list) {
                movie.title?.let { titleList.add(it) }
            }

            val autoCompleteMovieAdapter = AutoCompleteMovieAdapter(requireContext(), titleList)

            binding.autoComplete.setAdapter(autoCompleteMovieAdapter)
            binding.autoComplete.onItemClickListener =
                OnItemClickListener { parent, _, position, _ ->
                    val title = parent.getItemAtPosition(position) as String
                    Timber.d("title: $title")
                    val movie = viewModel.getCachedMovieByTitle(title)
                    movie?.let {
                        val action = MoviesFragmentDirections.actionMoviesFragmentToMovieDetailFragment(
                            it
                        )
                        findNavController().navigate(action)
                    }
                }
        })

        viewModel.getLoadDataError().observe(viewLifecycleOwner, {
            if (it) {
                binding.moviesList.visibility = View.GONE
                binding.loadProgress.visibility = View.GONE
                binding.loadErrorText.visibility = View.VISIBLE
            }
        })

        viewModel.getLoadInProgress().observe(viewLifecycleOwner, {
            if (it) {
                binding.moviesList.visibility = View.GONE
                binding.loadProgress.visibility = View.VISIBLE
                binding.loadErrorText.visibility = View.GONE
            }
        })

        viewModel.getMovies()
    }
}