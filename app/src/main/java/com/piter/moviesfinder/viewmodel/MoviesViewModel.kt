package com.piter.moviesfinder.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.piter.moviesfinder.data.MovieResult
import com.piter.moviesfinder.data.Result
import com.piter.moviesfinder.model.MovieApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class MoviesViewModel : ViewModel() {

    private val movieList = MutableLiveData<List<Result>>()
    private val searchList = MutableLiveData<List<Result>>()
    private val loadDataError = MutableLiveData<Boolean>()
    private val loadInProgress = MutableLiveData<Boolean>()

    private val disposable = CompositeDisposable()
    private val movieApiService = MovieApiService()

    fun getMovieList(): LiveData<List<Result>> = movieList
    fun getSearchList(): LiveData<List<Result>> = searchList
    fun getLoadDataError(): LiveData<Boolean> = loadDataError
    fun getLoadInProgress(): LiveData<Boolean> = loadInProgress

    fun getMovies() {
        loadInProgress.postValue(true)
        loadDataError.postValue(false)

        disposable.add(
            movieApiService.getMovies()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<MovieResult>() {
                    override fun onSuccess(movieResult: MovieResult) {
                        Timber.d("Success")
                        loadDataError.postValue(false)
                        loadInProgress.postValue(false)
                        movieList.postValue(movieResult.results)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e("error: %s", e.toString())
                        loadDataError.postValue(true)
                        loadInProgress.postValue(false)

                    }
                })
        )
    }

    fun searchMovie(title: String) {
        disposable.add(
            movieApiService.searchMovie(title)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<MovieResult>() {
                    override fun onSuccess(movieResult: MovieResult) {
                        Timber.d("Success")
                        searchList.postValue(movieResult.results)
                    }

                    override fun onError(e: Throwable) {
                        Timber.e("error: %s", e.toString())
                    }
                })
        )

    }

    fun getCachedMovieByTitle(title: String): Result? = searchList.value?.find { result -> result.title == title }
}